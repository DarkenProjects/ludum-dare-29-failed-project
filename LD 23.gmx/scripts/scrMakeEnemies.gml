if(!place_meeting(x,y,objPlayer) && !place_meeting(x,y,objEnemy) && !place_meeting(x,y,objFloor) && instance_number(objAmmoPickup) < 1 )
{
    if(random(20) < 1)
    {
        xx = x+choose(0,room_width);
        yy = y+choose(0,room_height);
        instance_create(xx,yy,objEnemy);
    }
    if(instance_number(objEnemy) = 10)
    {
        instance_destroy();
    }
    if(instance_number(objEnemy) < 0)
    {
        scrMakeEnemies();
    }
}
